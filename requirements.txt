# python 3.8.5

### pip requirements ###
arviz==0.11.2
corner==2.2.1
emcee==3.0.2
h5py==3.2.1
matplotlib==3.4.2
numba==0.53.1
numpy==1.20.1
pandas==1.2.4
pyFFTW==0.12.0
pyqt5==5.15.4
pytest==6.2.4
scipy==1.6.3
tqdm==4.60.0

### conda requirements ###
# faiss==1.7.0
# icc_rt==2020.2 (optional)