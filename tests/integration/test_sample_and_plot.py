import os

import numpy as np
import scipy.stats as stats

from src.mock import additive_gaussian, gen_mdof_walls, get_input, sim_output
from src.plot import plot_corner, plot_sample_hists, plot_sample_lines, plot_td_fd
from src.sample import EMCEESampler, GaussianNoiseLogVar, LogObsLik, LogPosterior, UniformLogPrior
from src.simulate import LogConverter, NewmarkSimulator
from src.utils import fft, fft_axis, MeasureType


def _plot_kwargs(label: str):
    return dict(show=False, file_path=os.path.join("figures", label))


def test_sample_and_plot():
    # =============== Setup =============== #
    np.random.seed(1)

    # simulate
    mdof = gen_mdof_walls([1, 1], [2, 2], [1000, 500])
    exc_point = 0
    obs_point = 0
    t, U = get_input(mdof, exc_point, "impulse", N=100, dt=1 / 100, k=5, size=1e3)
    z, x = sim_output(
        mdof, U, t, exc_point, obs_point, MeasureType.dis, noise_func=additive_gaussian, noise_kwargs=dict(var=1e-4)
    )

    # parameters
    params_true = np.concatenate((*mdof.modal_params(exc_point, obs_point), [np.log10(np.var(z - x))]))
    converter = LogConverter(np.arange(3 * mdof.p + 1) // mdof.p == 1)  # convert zeta to log domain
    theta_true = converter.to_theta(params_true)
    simulator = NewmarkSimulator(converter, t, U, exc_point, obs_point, MeasureType.dis)

    # distributions
    params_lb = np.array([12, 35, 1e-3, 1e-3, 0, 0, -6])
    params_ub = np.array([22, 45, 1e-1, 1e-1, 1, 1, -2])

    log_prior = UniformLogPrior(converter.to_theta(params_lb), converter.to_theta(params_ub))
    log_lik = LogObsLik(x, simulator, GaussianNoiseLogVar())
    log_post = LogPosterior(log_prior, log_lik)

    # =============== Sample =============== #
    hdf_file_path = "chain.hdf"
    sampler = EMCEESampler(log_post, 2000, log_post.log_prior.sample(20, seed=1), hdf_file_path=hdf_file_path)
    sampler.run(seed=1, show_progress=True, skip_initial_state_check=True)

    # print some stats
    print(theta_true)
    print(sampler.flat_acc_frac)
    print(sampler.flat_samples.mean(axis=0))
    print(sampler.flat_samples.std(axis=0))

    # =============== Plot =============== #
    os.makedirs("figures", exist_ok=True)

    # lines
    sampler.burn_in = 0
    sampler.thin = 1
    plot_sample_lines(sampler.samples, theta_true=theta_true, **_plot_kwargs("lines"))

    # histograms
    sampler.burn_in = 20
    sampler.thin = 5
    plot_sample_hists(
        sampler.flat_samples, theta_true=theta_true, pdf_funcs=log_post.log_prior.marg_pdfs, **_plot_kwargs("hists")
    )

    # corners
    plot_corner(sampler.flat_samples, group_by="mode", **_plot_kwargs("corner"))

    # MAP estimates
    x_hat = simulator(sampler.theta_opt(sampler.theta_max_all, verbose=True))
    fft_u = fft(U[exc_point, :])
    plot_td_fd(t, [x, x_hat], fft_axis(t), [fft(x) / fft_u, fft(x_hat) / fft_u], ["obs", "MAP"], **_plot_kwargs("map"))

    # model evidence via Laplace approximation
    lap_mean, lap_cov, lme = sampler.laplace_approx()
    print(f"Log model evidence (laplace) = {lme:.4g}")

    pdf_funcs = [stats.norm(lap_mean[i], lap_cov[i, i] ** 0.5).pdf for i in range(sampler.ndim)]
    plot_sample_hists(sampler.flat_samples[..., :-1], pdf_funcs=pdf_funcs[:-1], kde=True, **_plot_kwargs("lap"))
