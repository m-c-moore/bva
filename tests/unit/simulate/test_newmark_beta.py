import numpy as np
import pytest
import scipy.signal as signal
from scipy import linalg

from src.simulate.newmark import newmark_beta
from src.utils import alpha_to_Phi

EXC_POINT = 0


def _matrices_to_state_space(M, C, K, J=None, Ca=None, *, dt=None):
    """Return the state space model matrices `Ac`, `Bc`, `Cc` and `Dc`. Discrete if `dt` is given."""
    n = len(M)
    zeros = np.zeros((n, n))
    eye = np.eye(n)
    inv_M = np.diag(1 / np.diag(M))

    if J is None:
        J = eye
    if Ca is None:
        Ca = eye

    inv_M_K = inv_M @ K
    inv_M_C = inv_M @ C
    inv_M_J = inv_M @ J

    Ac = np.block([[zeros, eye], [-inv_M_K, -inv_M_C]])
    Bc = np.block([[zeros], [inv_M_J]])
    Cc = Ca @ np.block([[-inv_M_K, -inv_M_C]])
    Dc = Ca @ inv_M_J

    if dt is not None:
        Ad = linalg.expm(Ac * dt)
        Bd = (Ad - np.eye(len(Ad))) @ np.linalg.inv(Ac) @ Bc
        return Ad, Bd, Cc, Dc

    return Ac, Bc, Cc, Dc


def _sdof_matrices():
    return np.array([[1.0]]), np.array([[0.5]]), np.array([[100.0]])


def _mdof_matrices():
    return (
        np.diag([1.0, 2.0, 3.0]),
        np.array([[0.5, -0.2, 0.0], [-0.2, 0.3, -0.2], [0.0, -0.2, 0.3]]),
        np.array([[100.0, -100.0, 0.0], [-100.0, 200.0, -50.0], [0, -50.0, 100.0]]),
    )


def _sdof_params():
    return np.array([10]), np.array([0.1]), np.array([1])


def _mdof_params():
    return np.array([10, 20, 30]), np.array([0.1, 0.05, 0.02]), np.array([1, -1, 0.5])


def _simulate_and_compare(w, z, a, U, t, atol):
    # actual
    x_acc = newmark_beta(w, z, U, t, check_inputs=True)[0]

    M = np.eye(w.shape[0], dtype=np.float64)
    K = M * w ** 2
    C = np.diag(2.0 * z * w)

    Phi = alpha_to_Phi(a, EXC_POINT)

    # true
    Ac, Bc, Cc, Dc = _matrices_to_state_space(M, C, K)
    x_exp = signal.lsim(signal.StateSpace(Ac, Bc, Cc, Dc), U.T @ Phi, t)[2].T

    np.testing.assert_allclose(Phi.T @ x_acc, Phi.T @ x_exp[: x_acc.shape[0]], atol=atol)


@pytest.mark.parametrize("params", [_sdof_params, _mdof_params])
def test_newmark_beta_impulse(params):
    N = 1000
    dt = 0.005
    w, z, a = params()
    p = len(w)

    U = np.zeros((p, N))
    U[EXC_POINT, 3] = 100
    t = np.arange(0, N) * dt

    _simulate_and_compare(w, z, a, U, t, 1e-3)


@pytest.mark.parametrize("params", [_sdof_params, _mdof_params])
def test_newmark_beta_step(params):
    N = 1000
    dt = 0.005
    w, z, a = params()
    p = len(w)

    U = np.zeros((p, N))
    U[EXC_POINT, 3:] = 100
    t = np.arange(0, N) * dt

    _simulate_and_compare(w, z, a, U, t, 5e-3)


@pytest.mark.parametrize("params", [_sdof_params, _mdof_params])
def test_newmark_beta_random(params):
    N = 1000
    dt = 0.005
    w, z, a = params()
    p = len(w)

    U = np.zeros((p, N))
    U[EXC_POINT, :] = np.random.randn(N) * 100
    t = np.arange(0, N) * dt

    _simulate_and_compare(w, z, a, U, t, 1e-2)
