# Bayesian Vibration Analysis

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/m-c-moore%2Fbva/HEAD?filepath=example.ipynb) | [![pdoc3](https://img.shields.io/badge/Docs-pdoc3-red)](https://m-c-moore.gitlab.io/bva/src/) | [![pipelines](https://gitlab.com/m-c-moore/bva/badges/main/pipeline.svg)](https://gitlab.com/m-c-moore/bva/-/pipelines)

This repository contains the source code for my Engineering master's project, "[Application of Bayesian Inference Methods to Vibration Analysis](./report.pdf)".

Please refer to the [example notebook](./example.ipynb) for a quick overview of how it all works. It can be run online (without needing to install anything) on [binder](https://mybinder.org/v2/gl/m-c-moore%2Fbva/HEAD?filepath=example.ipynb). For a more detailed understanding, please refer to the [report](./report.pdf) or the [documentation](https://m-c-moore.gitlab.io/bva/src/).

## Example

Given 200 noisy observations of a three degree-of-freedom system:

![Observations](img/obs.svg)

and a set of uniform priors on the natural frequencies (left) and modal gains (right) (the priors on the damping ratios and observation noise variance were wide):

![Priors](img/priors.svg)

the program estimates the posterior probabilities of the modal parameters:

![Marginals](img/corner.svg)

The MAP estimates are shown in red. The program automatically selects and burns in converged chains. The simulations generated using some of the converged samples can be compared to the observations:

![Simulations](img/sim_td.svg)

Given multiple competing models, the program can compare them using the Bayes factor via the Laplace approximation.

## Tools used

- [emcee](https://github.com/dfm/emcee) - to sample from the posterior distributions.
- [corner.py](https://github.com/dfm/corner.py) - to generate nice plots of probability distributions.
- [pyFFTW](https://github.com/pyFFTW/pyFFTW) - to efficiently calculate FFTs.
- [pydvma](https://github.com/torebutlin/pydvma) - to record and analyse vibration data.
- [pdoc3](https://pdoc3.github.io/pdoc/) - to generate the [docs page](https://m-c-moore.gitlab.io/bva/src/).
