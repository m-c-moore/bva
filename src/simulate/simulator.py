"""File containing the abstract `Simulator` class."""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional

import numpy as np

from src.simulate.converters import Converter, DummyConverter


@dataclass
class Simulator(ABC):
    """Abstract base class from which simulators are extended."""

    converter: Optional[Converter]

    def __post_init__(self) -> None:
        if self.converter is None:
            self.converter = DummyConverter()

    def __call__(self, theta: np.ndarray) -> np.ndarray:
        return self._sim_func(self.converter.from_theta(theta))

    @abstractmethod
    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        pass
