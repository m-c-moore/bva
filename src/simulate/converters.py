"""Module containing classes to convert modal parameters to model parameters."""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import List

import numpy as np

from src.utils import MeasureType


@dataclass
class Converter(ABC):
    """Abstract base class from which classes defining parameter conversions are defined."""

    @abstractmethod
    def to_theta(self, params: np.ndarray, inplace: bool = False) -> np.ndarray:
        """Return the model parameters corresponding to the modal `params`."""
        pass

    @abstractmethod
    def from_theta(self, theta: np.ndarray, inplace: bool = False) -> np.ndarray:
        """Return the modal parameters corresponding to the model parameters `theta`."""
        pass

    @staticmethod
    def noise_var(theta: np.ndarray) -> float:
        """Extract the noise variance from the model parameters."""
        assert theta.shape[-1] % 3 >= 1
        return theta[..., -1]


@dataclass
class DummyConverter(Converter):
    """Converter that makes no changes to the parameters."""

    def to_theta(self, params: np.ndarray, inplace: bool = False) -> np.ndarray:
        return params

    def from_theta(self, theta: np.ndarray, inplace: bool = False) -> np.ndarray:
        return theta


@dataclass
class LogConverter(Converter):
    """Converts the specified modal parameters to the log domain (base 10)."""

    where: np.ndarray

    def to_theta(self, params: np.ndarray, inplace: bool = False) -> np.ndarray:
        return np.log10(params, where=self.where, out=params if inplace else params.copy())

    def from_theta(self, theta: np.ndarray, inplace: bool = False) -> np.ndarray:
        return np.power(10.0, theta, where=self.where, out=theta if inplace else theta.copy())


@dataclass
class ScaledConverter(Converter):
    """Converts the modal parameters by the given factors."""

    factors: np.ndarray

    def __post_init__(self) -> None:
        if not all(self.factors > 0):
            raise ValueError("Factor must be strictly positive")

    def to_theta(self, params: np.ndarray, inplace: bool = False) -> np.ndarray:
        return np.divide(params, self.factors, out=params if inplace else params.copy())

    def from_theta(self, theta: np.ndarray, inplace: bool = False) -> np.ndarray:
        return np.multiply(theta, self.factors, out=theta if inplace else theta.copy())


@dataclass
class AlphaConverter(Converter):
    """Converts `alpha` to `alpha / (2 * zeta)`. May include a factor of `omega` depending on `measure` type."""

    measure: MeasureType
    logged_zeta: bool = True

    def to_theta(self, params: np.ndarray, inplace: bool = False) -> np.ndarray:
        p = params.shape[-1] // 3
        z = 10.0 ** params[..., p : 2 * p] if self.logged_zeta else params[..., p : 2 * p]

        theta = params if inplace else params.copy()
        theta[..., 2 * p : 3 * p] /= 2 * z

        if self.measure == MeasureType.vel:
            theta[..., 2 * p : 3 * p] /= theta[..., :p]
        elif self.measure == MeasureType.dis:
            theta[..., 2 * p : 3 * p] /= theta[..., :p] ** 2

        return theta

    def from_theta(self, theta: np.ndarray, inplace: bool = False) -> np.ndarray:
        p = theta.shape[-1] // 3
        z = 10.0 ** theta[..., p : 2 * p] if self.logged_zeta else theta[..., p : 2 * p]

        params = theta if inplace else theta.copy()
        params[..., 2 * p : 3 * p] *= 2 * z

        if self.measure == MeasureType.vel:
            params[..., 2 * p : 3 * p] *= params[..., :p]
        elif self.measure == MeasureType.dis:
            params[..., 2 * p : 3 * p] *= params[..., :p] ** 2

        return params


@dataclass
class CompoundConverter(Converter):
    """Container for multiple converters. Be careful of the order."""

    converters: List[Converter]

    def __getitem__(self, key: int) -> Converter:
        return self.converters[key]

    def to_theta(self, params: np.ndarray, inplace: bool = False) -> np.ndarray:
        theta = params if inplace else params.copy()

        for c in self.converters:
            c.to_theta(theta, True)

        return theta

    def from_theta(self, theta: np.ndarray, inplace: bool = False) -> np.ndarray:
        params = theta if inplace else theta.copy()

        for c in self.converters[::-1]:
            c.from_theta(params, True)

        return params
