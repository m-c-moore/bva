"""File containing functions and simulators to run the Newmark-beta integrator [1]_.

The inline comment numbers correspond to the equation numbers in [2]_.

References
----------
.. [1] Newmark (1959) http://ascelibrary.org/doi/10.1061/JMCEA3.0000098
.. [2] George Lindfield and John Penny (2019) https://doi.org/10.1016/C2016-0-00395-9
"""

from dataclasses import dataclass, field
from typing import Tuple

import numpy as np
from numba import jit

from src.simulate.simulator import Simulator
from src.utils import alpha_to_Phi, MeasureType, split_modal_params


@jit(nopython=True)
def _calc_matrices(
    w_n: np.ndarray, z_n: np.ndarray, beta: float, gamma: float, dt: float
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    M = np.identity(len(w_n))
    C = 2 * z_n * w_n * M  # product is faster than np.diag

    _a = beta * dt ** 2
    _b = 2 * gamma * dt
    inv_A = M * (_a / (1 + _b * z_n * w_n + _a * w_n ** 2))

    return M, C, inv_A


def newmark_beta(
    w_n: np.ndarray, z_n: np.ndarray, U: np.ndarray, t: np.ndarray, *, check_inputs: bool = False
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Implementation of the Newmark-beta algorithm, optimised to work with decoupled mode shapes.

    The following assumptions are made:
        - The mass matrix `M` is the identity matrix;
        - The damping matrix `C` equals `diag(2 * z_n * w_n)`;
        - The stiffness matrix `K` equals `diag(w_n ** 2)`;
        - `u` is the generalised input.

    Parameters
    ----------
    w_n
        Natural frequencies, length p.
    z_n
        Modal damping ratios, length p.
    U
        Matrix of generalised inputs, p x N.
    t
        Array of time points, length N. Must be evenly spaced.
    check_inputs
        If set to `True`, will check that the inputs are the correct shape.

    Returns
    -------
    x, xd, xdd
        The arrays containing the displacement, velocity and acceleration values at each time step.
    """
    gamma = 0.5
    beta = 0.25

    p = len(w_n)
    N = len(t)
    dt = (t[-1] - t[0]) / (N - 1)

    if check_inputs:
        assert (p,) == w_n.shape == z_n.shape, "Modal parameters should be length p"
        assert (p, N) == U.shape, "U should be p x N"
        np.testing.assert_allclose(np.diff(t), dt), "Time axis t should be uniformly spaced"

    # initialise outputs
    x = np.zeros((p, N))
    xd = np.zeros_like(x)
    xdd = np.zeros_like(x)
    xdd[:, 0] = U[:, 0].copy()

    # run algorithm
    M, C, inv_A = _calc_matrices(w_n, z_n, beta, gamma, dt)
    _newmark_run(x, xd, xdd, M, C, U, N, dt, inv_A, beta, gamma)
    return x, xd, xdd


@jit(nopython=True, cache=True)
def _newmark_run(
    x: np.ndarray,
    xd: np.ndarray,
    xdd: np.ndarray,
    M: np.ndarray,
    C: np.ndarray,
    U: np.ndarray,
    N: int,
    dt: float,
    inv_A: np.ndarray,
    beta: float,
    gamma: float,
) -> None:
    """Run the Newmark-beta integrator. Modifies `X` in place.

    This function has been written to allow it to be compiled by Numba's JIT compiler, allowing it to run
    almost 20x faster (excluding compilation time).
    """
    _b = 1 / (beta * dt)
    _a = _b / dt
    _c = 1 / (2 * beta) - 1

    _d = _b * gamma
    _e = 1 - gamma / beta
    _f = (1 - gamma / (2 * beta)) * dt

    _g = (1 - gamma) * dt
    _h = gamma * dt

    for i in range(N - 1):
        B = (
            U[:, i + 1]
            + M @ (_a * x[:, i] + _b * xd[:, i] + _c * xdd[:, i])
            + C @ (_d * x[:, i] - _e * xd[:, i] - _f * xdd[:, i])
        )  # 5.47

        x[:, i + 1] = inv_A @ B
        xdd[:, i + 1] = _a * (x[:, i + 1] - x[:, i]) - _b * xd[:, i] - _c * xdd[:, i]  # 5.42
        xd[:, i + 1] = xd[:, i] + _g * xdd[:, i] + _h * xdd[:, i + 1]  # 5.39


@dataclass
class NewmarkSimulator(Simulator):
    """Class to simulate time domain responses using the `newmark_beta_dc` function."""

    t: np.ndarray = field(repr=False)
    U: np.ndarray = field(repr=False)
    exc_point: int
    obs_point: int
    measure: MeasureType

    def __post_init__(self) -> None:
        super().__post_init__()

        assert all(self.t >= 0)
        np.testing.assert_allclose(np.diff(self.t), self.t[1] - self.t[0])  # uniform sampling

        assert 0 <= self.exc_point < self.U.shape[0]
        assert 0 <= self.obs_point < self.U.shape[0]
        assert self.t.shape[0] == self.U.shape[1]
        assert np.round(np.sum(np.abs(np.delete(self.U, self.exc_point, 0))), 8) == 0

        self.i = self.measure.value

    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        omega, zeta, alpha = split_modal_params(params)
        Phi = alpha_to_Phi(alpha, self.exc_point)
        return np.sum(newmark_beta(omega, zeta, Phi.T @ self.U, self.t)[self.i], axis=0)


@dataclass
class InterpolatedNewmarkSimulator(NewmarkSimulator):
    """Class to simulate time domain responses using the `newmark_beta_dc` function.

    Will interpolate the time axis by the given factor to increase accuracy.
    """

    factor: int

    def __post_init__(self) -> None:
        super().__post_init__()
        assert 1 < self.factor

        N = (len(self.t) - 1) * self.factor + 1
        t = np.linspace(self.t[0], self.t[-1], N)
        u = np.interp(t, self.t, self.U[self.exc_point])
        U = np.zeros((self.U.shape[0], N))
        U[0, :] = u

        self.t = t
        self.U = U

    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        return super()._sim_func(params)[:: self.factor]
