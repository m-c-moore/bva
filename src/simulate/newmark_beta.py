"""File containing functions to run the Newmark-beta integrator.

The inline comment numbers correspond to the equation numbers in [1].

References
----------
[1] - "Numerical Methods Using MATLAB", George Lindfield and John Penny. https://doi.org/10.1016/C2016-0-00395-9
"""

from typing import Optional, Tuple

import numpy as np
from numba import jit


@jit(nopython=True)
def _calc_matrices(
    w_n: np.ndarray, z_n: np.ndarray, beta: float, gamma: float, dt: float
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    M = np.identity(len(w_n))
    C = 2 * z_n * w_n * M  # product is faster than np.diag

    _a = beta * dt ** 2
    _b = 2 * gamma * dt
    inv_A = M * (_a / (1 + _b * z_n * w_n + _a * w_n ** 2))

    return M, C, inv_A


def newmark_beta_dc(
    w_n: np.ndarray, z_n: np.ndarray, U: np.ndarray, t: np.ndarray, *, check_inputs: bool = False
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Implementation of the Newmark-beta algorithm, optimised to work with decoupled mode shapes.

    The following assumptions are made:
        - The mass matrix `M` is the identity matrix;
        - The damping matrix `C` equals `diag(2 * z_n * w_n)`;
        - The stiffness matrix `K` equals `diag(w_n ** 2)`;
        - `u` is the generalised input.

    Parameters
    ----------
    w_n
        Natural frequencies, length p.
    z_n
        Modal damping ratios, length p.
    U
        Matrix of generalised inputs, p x N.
    t
        Array of time points, length N. Must be evenly spaced.
    check_inputs
        If set to `True`, will check that the inputs are the correct shape.

    Returns
    -------
    x, xd, xdd
        The arrays containing the displacement, velocity and acceleration values at each time step.
    """
    gamma = 0.5
    beta = 0.25

    p = len(w_n)
    N = len(t)
    dt = (t[-1] - t[0]) / (N - 1)

    if check_inputs:
        assert (p,) == w_n.shape == z_n.shape, "Modal parameters should be length p"
        assert (p, N) == U.shape, "U should be p x N"
        np.testing.assert_allclose(np.diff(t), dt), "Time axis t should be uniformly spaced"

    # initialise outputs
    x = np.zeros((p, N))
    xd = np.zeros_like(x)
    xdd = np.zeros_like(x)
    xdd[:, 0] = U[:, 0].copy()

    # run algorithm
    M, C, inv_A = _calc_matrices(w_n, z_n, beta, gamma, dt)
    _newmark_run(x, xd, xdd, M, C, U, N, dt, inv_A, beta, gamma)
    return x, xd, xdd


def newmark_beta(
    M: np.ndarray,
    C: np.ndarray,
    K: np.ndarray,
    U: np.ndarray,
    t: np.ndarray,
    x_0: Optional[np.ndarray] = None,
    xd_0: Optional[np.ndarray] = None,
    *,
    check_inputs: bool = False,
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Implementation of the Newmark-beta algorithm.

    Parameters
    ----------
    M
        Mass matrix, p x p. Must have dtype=np.float64.
    C
        Damping matrix, p x p. Must have dtype=np.float64.
    K
        Stiffness matrix, p x p. Must have dtype=np.float64.
    U
        Matrix of inputs, p x N. Must have dtype=np.float64.
    t
        Array of time points, length N. Must be evenly spaced.
    x_0
        Initial displacements, length p. Default is 0.
    xd_0
        Initial velocities, length p. Default is 0.
    check_inputs
        If set to `True`, will check that the inputs are the correct shape.

    Returns
    -------
    x, xd, xdd
        The arrays containing the displacement, velocity and acceleration values at each time step.
    """
    gamma = 0.5
    beta = 0.25

    p = len(M)
    N = len(t)
    dt = (t[-1] - t[0]) / (N - 1)

    if check_inputs:
        assert (p, p) == M.shape == C.shape == K.shape, "Dynamic matrices should be p x p"
        assert (p, N) == U.shape, "U should be p x N"
        np.testing.assert_allclose(np.diff(t), dt), "Time axis t should be uniformly spaced"

        if x_0 is not None:
            assert (p,) == x_0.shape, "Initial displacements should be length p"
        if xd_0 is not None:
            assert (p,) == xd_0.shape, "Initial velocities should be length p"

    # initialise outputs
    x = np.zeros((p, N))
    xd = np.zeros_like(x)
    xdd = np.zeros_like(x)

    # initial conditions
    if x_0 is not None:
        x[:, 0] = x_0
    if xd_0 is not None:
        xd[:, 0] = xd_0
    xdd[:, 0] = np.linalg.solve(M, U[:, 0] - C @ xd[:, 0] - K @ x[:, 0])

    # calculate A and its inverse
    A = M / (beta * dt ** 2) + gamma * C / (beta * dt) + K  # 5.46
    inv_A = np.linalg.inv(A)

    _newmark_run(x, xd, xdd, M, C, U, N, dt, inv_A, beta, gamma)
    return x, xd, xdd


@jit(nopython=True, cache=True)
def _newmark_run(
    x: np.ndarray,
    xd: np.ndarray,
    xdd: np.ndarray,
    M: np.ndarray,
    C: np.ndarray,
    U: np.ndarray,
    N: int,
    dt: float,
    inv_A: np.ndarray,
    beta: float,
    gamma: float,
) -> None:
    """Run the Newmark-beta integrator. Modifies `X` in place.

    This function has been written to allow it to be compiled by Numba's JIT compiler, allowing it to run
    almost 20x faster (excluding compilation time).
    """
    _b = 1 / (beta * dt)
    _a = _b / dt
    _c = 1 / (2 * beta) - 1

    _d = _b * gamma
    _e = 1 - gamma / beta
    _f = (1 - gamma / (2 * beta)) * dt

    _g = (1 - gamma) * dt
    _h = gamma * dt

    for i in range(N - 1):
        B = (
            U[:, i + 1]
            + M @ (_a * x[:, i] + _b * xd[:, i] + _c * xdd[:, i])
            + C @ (_d * x[:, i] - _e * xd[:, i] - _f * xdd[:, i])
        )  # 5.47

        x[:, i + 1] = inv_A @ B
        xdd[:, i + 1] = _a * (x[:, i + 1] - x[:, i]) - _b * xd[:, i] - _c * xdd[:, i]  # 5.42
        xd[:, i + 1] = xd[:, i] + _g * xdd[:, i] + _h * xdd[:, i + 1]  # 5.39
