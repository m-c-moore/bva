"""File containing classes used to simulate responses from modal parameters."""

from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Optional

import numpy as np

from src.simulate.converters import Converter, DummyConverter
from src.simulate.modal_sum import modal_sum_db, modal_sum_raw
from src.simulate.newmark_beta import newmark_beta_dc
from src.utils import alpha_to_Phi, split_modal_params
from src.utils.constants import MeasureType


@dataclass
class Simulator(ABC):
    """Abstract base class from which simulators are extended."""

    converter: Optional[Converter]

    def __post_init__(self) -> None:
        if self.converter is None:
            self.converter = DummyConverter()

    def __call__(self, theta: np.ndarray) -> np.ndarray:
        return self._sim_func(self.converter.from_theta(theta))

    @abstractmethod
    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        pass


@dataclass
class NewmarkSimulator(Simulator):
    """Class to simulate time domain responses using the `newmark_beta_dc` function."""

    t: np.ndarray = field(repr=False)
    U: np.ndarray = field(repr=False)
    exc_point: int
    obs_point: int
    measure: MeasureType

    def __post_init__(self) -> None:
        super().__post_init__()

        assert all(self.t >= 0)
        np.testing.assert_allclose(np.diff(self.t), self.t[1] - self.t[0])  # uniform sampling

        assert 0 <= self.exc_point < self.U.shape[0]
        assert 0 <= self.obs_point < self.U.shape[0]
        assert self.t.shape[0] == self.U.shape[1]
        assert np.round(np.sum(np.abs(np.delete(self.U, self.exc_point, 0))), 8) == 0

        self.i = self.measure.value

    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        omega, zeta, alpha = split_modal_params(params)
        Phi = alpha_to_Phi(alpha, self.exc_point)
        return np.sum(newmark_beta_dc(omega, zeta, Phi.T @ self.U, self.t)[self.i], axis=0)


@dataclass
class InterpolatedNewmarkSimulator(NewmarkSimulator):
    """Class to simulate time domain responses using the `newmark_beta_dc` function.

    Will interpolate the time axis by the given factor to increase accuracy.
    """

    factor: int

    def __post_init__(self) -> None:
        super().__post_init__()
        assert 1 < self.factor

        N = (len(self.t) - 1) * self.factor + 1
        t = np.linspace(self.t[0], self.t[-1], N)
        u = np.interp(t, self.t, self.U[self.exc_point])
        U = np.zeros((self.U.shape[0], N))
        U[0, :] = u

        self.t = t
        self.U = U

    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        return super()._sim_func(params)[:: self.factor]


@dataclass
class ModalSimulator(Simulator):
    """Class to simulate frequency domain responses using the `modal_sum_raw` function."""

    w: np.ndarray
    u_fft: np.ndarray

    def __post_init__(self) -> None:
        super().__post_init__()

        assert all(self.w >= 0)
        assert self.w.shape == self.u_fft.shape

    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        tf = modal_sum_raw(self.w, *split_modal_params(params))
        return np.concatenate([np.real(tf), np.imag(tf)])


@dataclass
class ModalDBSimulator(ModalSimulator):
    """Class to simulate frequency domain responses using the `modal_sum_db` function."""

    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        return modal_sum_db(self.w, *split_modal_params(params))
