"""File containing functions and simulators to generate transfer functions using the modal summation formula."""

from dataclasses import dataclass

import numpy as np
from numba import jit

from src.simulate.simulator import Simulator
from src.utils import MeasureType, split_modal_params


@jit(nopython=True)
def modal_sum(w: np.ndarray, w_n: np.ndarray, z_n: np.ndarray, a_n: np.ndarray) -> np.ndarray:
    """Return the simulated complex transfer function."""
    tf = np.zeros_like(w, dtype=np.complex64)
    for i in range(len(w_n)):
        tf += a_n[i] / (w_n[i] ** 2 + 2j * w_n[i] * z_n[i] * w - w ** 2)
    return tf


@dataclass
class ModalSimulator(Simulator):
    """Class to simulate frequency domain responses using the `modal_sum_raw` function."""

    w: np.ndarray
    u_fft: np.ndarray
    measure: MeasureType

    def __post_init__(self) -> None:
        super().__post_init__()

        assert all(self.w >= 0)
        assert self.w.shape == self.u_fft.shape

    def _sim_func(self, params: np.ndarray) -> np.ndarray:
        tf = modal_sum(self.w, *split_modal_params(params))
        if self.measure == MeasureType.vel:
            tf *= self.w * 1j
        elif self.measure == MeasureType.acc:
            tf *= -self.w ** 2
        return tf
