"""File containing functions to simulate a transfer function using the modal summation formula."""

import numpy as np
from numba import jit

from src.utils import to_db


@jit(nopython=True)
def modal_sum_raw(w: np.ndarray, w_n: np.ndarray, z_n: np.ndarray, a_n: np.ndarray) -> np.ndarray:
    """Return the simulated complex transfer function."""
    r = np.zeros_like(w, dtype=np.complex64)
    for i in range(len(w_n)):
        r += a_n[i] / (w_n[i] ** 2 + 2j * w_n[i] * z_n[i] * w - w ** 2)
    return r


@jit(nopython=True)
def modal_sum_db(w: np.ndarray, w_n: np.ndarray, z_n: np.ndarray, a_n: np.ndarray) -> np.ndarray:
    """Return the simulated transfer function in decibels."""
    return to_db(modal_sum_raw(w, w_n, z_n, a_n))
