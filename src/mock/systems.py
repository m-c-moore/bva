"""File containing the `MDOFSystem` class and several helper functions to initialise instances of it."""

from typing import List, Tuple

import numpy as np
import scipy.linalg as linalg

from src.simulate.newmark import newmark_beta
from src.utils import alpha_to_Phi, MeasureType


class MDOFSystem:
    """Class defining a multiple degree-of-freedom system."""

    def __init__(self, M: np.ndarray, C: np.ndarray, K: np.ndarray):
        """Initialise an `MDOFSystem` instance.

        Parameters
        ----------
        M
            Mass matrix.
        C
            Damping matrix.
        K
            Stiffness matrix.
        """
        self.p = M.shape[0]
        self.M = M
        self.C = C
        self.K = K

        # noinspection PyTupleAssignmentBalance
        self.omega, self.Phi = linalg.eigh(K, M)
        self.omega = np.nan_to_num(np.sqrt(np.real(self.omega)), nan=0.0)

        # perform checks
        eye = np.eye(self.p)
        PMP = self.Phi.T @ self.M @ self.Phi
        PCP = self.Phi.T @ self.C @ self.Phi
        PKP = self.Phi.T @ self.K @ self.Phi

        np.testing.assert_allclose(PMP, eye, atol=1e-9)
        np.testing.assert_allclose(PKP, eye * PKP, atol=1e-9)

        # calculate modal damping ratios
        self.zeta = np.diag(PCP) / (2 * self.omega)

        # sort by frequency ascending
        idx = np.argsort(self.omega)
        self.omega = self.omega[idx]
        self.zeta = self.zeta[idx]
        self.Phi = self.Phi[:, idx]

    def alpha(self, i: int, j: int) -> np.ndarray:
        """Return the true value for `alpha` given excitation and observation points."""
        return self.Phi[i, :] * self.Phi[j, :]

    def modal_params(self, i: int, j: int) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Return the true value for `theta` given excitation and observation points."""
        return self.omega, self.zeta, self.alpha(i, j)

    def simulate(self, U: np.ndarray, t: np.ndarray, i: int, j: int, measure: MeasureType) -> np.ndarray:
        """Return the simulated output of the system to an input `U`."""
        Phi = alpha_to_Phi(self.alpha(i, j), i)
        return np.sum(newmark_beta(self.omega, self.zeta, Phi.T @ U, t)[measure.value], axis=0)


def gen_mdof_independent(omega: np.ndarray, zeta: np.ndarray) -> MDOFSystem:
    """Return an `MDOFSystem` formed of independent masses and springs."""
    M = np.eye(omega.shape[0], dtype=np.float64)
    K = M * omega ** 2
    C = np.diag(2.0 * zeta * omega)

    return MDOFSystem(M=M, C=C, K=K)


def gen_sdof(omega: float, zeta: float) -> MDOFSystem:
    """Return an `MDOFSystem` for a single degree of freedom system."""
    return gen_mdof_independent(np.array([omega]), np.array([zeta]))


def gen_mdof_walls(m: List[float], c: List[float], k: List[float]) -> MDOFSystem:
    """Return an `MDOFSystem` of the form `|~~[m]~~[m]~...` or `|~~[m]~~[m]~...~|`."""
    _m = m.copy()
    _c = c.copy()
    _k = k.copy()
    n = len(_m)

    if len(_k) == len(_c) == n:
        _k += [0]
        _c += [0]
    elif not (len(_k) == len(_c) == n + 1):
        raise ValueError("k and/or c have invalid length.")

    M = np.diag(_m).astype(np.float64)
    C = np.zeros((n, n), dtype=np.float64)
    K = C.copy()

    for i in range(n):
        C[i, i] = _c[i] + _c[i + 1]
        K[i, i] = _k[i] + _k[i + 1]

        if i != 0:
            C[i, i - 1] = C[i - 1, i] = -_c[i]
            K[i, i - 1] = K[i - 1, i] = -_k[i]

    return MDOFSystem(M=M, C=C, K=K)
