"""File containing the `sim_output` function."""

from typing import Any, Callable, Dict, Optional, Tuple, TYPE_CHECKING

import numpy as np

from src.simulate.newmark import newmark_beta
from src.utils import alpha_to_Phi, fft, MeasureType, to_db

if TYPE_CHECKING:
    from src.mock.systems import MDOFSystem


def sim_output(
    mdof: "MDOFSystem",
    U: np.ndarray,
    t: np.ndarray,
    exc_point: int,
    obs_point: int,
    measure: MeasureType,
    fd: bool = False,
    *,
    noise_func: Optional[Callable] = None,
    noise_kwargs: Optional[Dict[str, Any]] = None,
    filter_func: Optional[Callable] = None,
    filter_kwargs: Optional[Dict[str, Any]] = None,
) -> Tuple[np.ndarray, np.ndarray]:
    """Generate and return the noise free and noisy observations.

    Parameters
    ----------
    mdof
        `MDOFSystem` instance.
    U
        Excitation matrix.
    t
        Time axis.
    exc_point
        Index of the excitation point.
    obs_point
        Index of the observation point.
    measure
        `MeasureType` value.
    fd
        Set to `True` to return the observations in the frequency domain.
    noise_func
        Function used to add noise to the output.
    noise_kwargs
        Keyword arguments to pass to `noise_func`.
    filter_func
        Function used to filter the noisy output.
    filter_kwargs :
        Keyword arguments to pass to `filter_func`.
    """
    if not np.argwhere(np.sum(U, axis=1) != 0.0).shape == (1, 1):
        raise ValueError("Must have exactly one excitation point")

    # simulate
    omega = mdof.omega.copy()
    zeta = mdof.zeta.copy()
    alpha = mdof.alpha(exc_point, obs_point)
    U = U.copy()

    exc_points = np.argwhere(np.sum(U, axis=1) != 0.0)
    if not exc_points.shape == (1, 1):
        raise ValueError("Must have exactly one excitation point")

    Phi = alpha_to_Phi(alpha, exc_points[0, 0])
    Z = newmark_beta(omega, zeta, Phi.T @ U, t)[measure.value]
    z: np.ndarray = np.sum(Z, axis=0)
    x = z.copy()

    # add noise
    if noise_func is not None:
        if noise_kwargs is None:
            noise_kwargs = {}
        x[:] = noise_func(x, **noise_kwargs)

    # filter
    if filter_func is not None:
        if filter_kwargs is None:
            filter_kwargs = {}
        x[:] = filter_func(x, dt=t[1] - t[0], **filter_kwargs)

    if fd:
        fft_u = fft(U[exc_point, :])
        return to_db(fft(z) / fft_u), to_db(fft(x) / fft_u)

    return z, x
