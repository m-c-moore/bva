"""File containing the `low_pass_butter` function."""

import warnings

import numpy as np
from scipy.signal import butter, filtfilt


def low_pass_butter(x: np.ndarray, dt: float, cutoff: float, order: int = 4) -> np.ndarray:
    """Apply a Butterworth digital filter forward and backward to a signal.

    Parameters
    ----------
    x
        Input data.
    dt
        Sampling period in seconds.
    cutoff
        Cutoff frequency in Hz.
    order
        Order of the filter. Will be divided by 2 in `scipy.signal.butter()`.
    """
    nyq_freq = 0.5 / dt
    normal_cutoff = cutoff / nyq_freq

    if normal_cutoff > 1:
        warnings.warn(f"Cutoff reduced from {normal_cutoff} to 0.99")
        normal_cutoff = 0.99

    b, a = butter(order // 2, normal_cutoff, btype="low", analog=False, output="ba")  # noqa
    return filtfilt(b, a, x)
