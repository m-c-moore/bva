"""File containing functions to add different types of noise to signals."""

import numpy as np


def additive_gaussian(x: np.ndarray, var: float, mean: float = 0) -> np.ndarray:
    """Return the amplitudes with added Gaussian noise."""
    return x + mean + np.random.randn(*x.shape) * (var ** 0.5)


def snr_to_var(x: np.ndarray, snr: float, db: bool = False) -> float:
    """Return the variance of the noise needed to achieve the given SNR.

    If `db` is set `True`, `snr` should be given in decibels.
    """
    if x.ndim != 1:
        raise ValueError("x must be one dimensional")
    if db:
        snr = 10 ** (snr / 10)

    L = 1  # oversampling factor
    sig_pow = L * np.sum(x ** 2) / x.shape[0]
    return sig_pow / snr


def additive_gaussian_snr(x: np.ndarray, snr: float, db: bool = False, mean: float = 0) -> np.ndarray:
    """Return the amplitudes with added Gaussian noise.

    The variance of the noise is set by the given SNR. If `db` is set `True`, `snr`
    should be given in decibels.
    """
    return additive_gaussian(x, snr_to_var(x, snr, db), mean)


def additive_uniform(x: np.ndarray, width: float, mean: float = 0) -> np.ndarray:
    """Return the amplitudes with added random uniform noise."""
    return x + mean + (np.random.rand(*x.shape) - 0.5) * width


def additive_sinusoid(x: np.ndarray, t: np.ndarray, omega: float, amplitude: float, phase_shift: float) -> np.ndarray:
    """Return the amplitudes with added sinusoidal noise."""
    return x + amplitude * np.sin(omega * t + phase_shift)
