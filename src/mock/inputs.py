"""File containing the `get_input` function."""

from typing import Any, Callable, Dict, Optional, Tuple, TYPE_CHECKING

import numpy as np

if TYPE_CHECKING:
    from src.mock.systems import MDOFSystem


def get_input(
    mdof: "MDOFSystem",
    exc_point: int,
    input_type: str,
    N: int,
    dt: float,
    k: int = 0,
    k2: Optional[int] = None,
    size: float = 100,
    noise_filter: Optional[Callable] = None,
    noise_filter_kwargs: Optional[Dict[str, Any]] = None,
    sinusoid_freq: float = 2 * np.pi,
) -> Tuple[np.ndarray, np.ndarray]:
    """Return the time axis and excitation matrix representing the input to a dynamic system.

    Parameters
    ----------
    mdof
        `MDOFSystem` instance.
    exc_point
        Index of the excitation point.
    input_type
        One of "impulse", "step", "noise", "sinusoid", "chirp"
    N
        Number of points.
    dt
        Time step in seconds.
    k
        Index of the point at which the input starts. Default is 0.
    k2
        Index of the point at which the input ends. Default is N.
    size
        Magnitude of the input.
    noise_filter
        Function used to filter the noise input.
    noise_filter_kwargs
        Keyword arguments to pass to `noise_filter`.
    sinusoid_freq
        Frequency of the "sinusoid" input or the max frequency of the "chirp" input.
    """
    if noise_filter_kwargs is None:
        noise_filter_kwargs = dict()

    t = np.arange(0, N) * dt
    U = np.zeros((mdof.p, N))

    if k2 is None:
        k2 = N

    if input_type == "impulse":
        U[exc_point, k] = size
    elif input_type == "step":
        U[exc_point, k:] = size
    elif input_type == "noise":
        r = size * np.random.randn(k2 - k)
        if noise_filter is not None:
            r = noise_filter(r, dt=dt, **noise_filter_kwargs)
        U[exc_point, k:k2] = r
    elif input_type == "sinusoid":
        U[exc_point, k:k2] = size * np.sin(sinusoid_freq * t)[: k2 - k]
    elif input_type == "chirp":
        U[exc_point, k:k2] = size * np.sin(sinusoid_freq / (2 * t[k2 - k]) * t ** 2)[: k2 - k]
    else:
        raise ValueError(input_type)

    return t, U
