"""Utility functions for the `plot` module."""

import os
from collections import defaultdict
from typing import Any, DefaultDict, Iterator, List, Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np

_PREV_FIGS: DefaultDict[str, int] = defaultdict(int)
TAxes = Union[List[np.ndarray], np.ndarray]


def new_fig(
    default_name: str,
    figsize: Tuple[float, float],
    file_path: Optional[str] = None,
    replace: bool = False,
    **kwargs: Any,
) -> plt.Figure:
    if file_path is not None:
        name = os.path.split(file_path)[1].split(".")[0]
    else:
        _name = kwargs.get("name", default_name)
        if not replace:
            _PREV_FIGS[_name] += 1
        name = f"{_name}:{_PREV_FIGS[_name]}"

    fig = plt.figure(name, figsize=figsize)
    fig.clear()
    return fig


def iterate_args(
    x: Optional[TAxes], y: TAxes, names: Optional[Union[List[Optional[str]], str]]
) -> Iterator[Tuple[np.ndarray, np.ndarray, Optional[str]]]:
    if not isinstance(y, list):
        if y.ndim == 2:
            y = list(y)
        else:
            y = [y]

    n = len(y)
    if not isinstance(x, list):
        if x is not None and x.ndim == 2:
            x = list(x)
        else:
            x = [x] * n

    if names is None:
        names = [None] * n
    elif isinstance(names, str):
        names = [names] + [None] * (n - 1)

    return zip(x, y, names)


def save_and_show(file_path: Optional[str] = None, show: bool = True, **kwargs: Any) -> None:
    if file_path is not None:
        save_kwargs = dict(bbox_inches="tight")
        if "." not in os.path.split(file_path)[1]:
            plt.savefig(file_path + ".svg", **save_kwargs)
        else:
            plt.savefig(file_path, **save_kwargs)
    if show:
        plt.show()
    elif file_path is not None:  # only close "final" versions
        plt.close()
