"""File containing functions used to plot MCMC samples."""

from typing import Any, Callable, List, Optional, Tuple, Union

import corner
import numpy as np
from matplotlib import pyplot as plt
from scipy.stats import gaussian_kde

from src.plot.utils import iterate_args, new_fig, save_and_show, TAxes

# TODO convert to enums
_NAMES = ("omega", "log_zeta", "alpha_2zeta}")
_TEX_NAMES = ("\\omega", "\\log_{10} \\zeta", "\\alpha~/~2\\zeta")
_HAT_NAMES = ("\\hat{\\omega}", "\\log_{10} \\hat{\\zeta}", "\\hat{\\alpha}~/~2\\hat{\\zeta}")
_I_HAT_NAMES = ("\\hat{\\omega_{#}}", "\\log_{10} \\hat{\\zeta_{#}}", "\\hat{\\alpha_{#}}~/~2\\hat{\\zeta_{#}}")
_COLOURS = ("C0", "C1", "C2", "C4", "C5", "C6", "C7", "C8", "C9")


def plot_sample_line(
    y: TAxes,
    x: Optional[TAxes] = None,
    names: Optional[Union[List[Optional[str]], str]] = None,
    colour: Optional[str] = None,
    alpha: float = 1,
    ylabel: Optional[str] = None,
    ylim: Optional[Tuple[int, int]] = None,
    hline: Optional[float] = None,
    vline: Optional[float] = None,
    new: bool = True,
    **plot_kwargs: Any,
) -> None:
    if new:
        new_fig("line", (7, 4), **plot_kwargs)

    for _x, _y, _name in iterate_args(x, y, names):
        _x = np.arange(1, len(_y) + 1) if _x is None else _x

        # don't need to plot every point
        if len(_y) > 1000:
            _y = _y[:: len(_y) // 1000]
            _x = _x[:: len(_x) // 1000]

        plt.plot(_x, _y, label=_name, color=colour, alpha=alpha)

    if names is not None:
        plt.legend()

    if ylim is not None:
        plt.ylim(*ylim)

    if hline is not None:
        plt.gca().axhline(hline, color="k", linestyle="--")

    if vline is not None:
        plt.gca().axvline(vline, color="k", linestyle="--")

    plt.xlabel("Iteration")
    plt.ylabel(ylabel)
    plt.grid(True)
    save_and_show(**plot_kwargs)


def plot_sample_lines(
    samples: np.ndarray,
    lp_samples: Optional[np.ndarray] = None,
    theta_true: Optional[np.ndarray] = None,
    **plot_kwargs: Any,
) -> None:
    p = samples.shape[-1] // 3
    fig = new_fig("lines", (12, 6), **plot_kwargs)

    # make transparent if multiple chains
    alpha = 0.3 if samples.ndim == 3 else 1

    # parameter samples
    for i, name in enumerate(_HAT_NAMES):
        fig.add_subplot(2, 2, i + 1)

        for j in range(p):
            plot_sample_line(
                samples[..., i * p + j],
                name=f"mode {j+1}" if i == 0 else None,
                colour=_COLOURS[j % len(_COLOURS)],
                alpha=alpha,
                new=False,
                show=False,
            )

        if theta_true is not None:
            for j in range(p):
                plt.axhline(theta_true[i * p + j], linestyle="--", color="C3")

        plt.ylabel(f"${name}$")
        if i < 2:
            plt.xlabel(None)
            plt.xticks(visible=False)

    # log posterior samples
    fig.add_subplot(2, 2, 4)
    if lp_samples is None:
        plot_sample_line(
            samples[..., -1], ylabel="$\\log_{10} \\hat{\\sigma}_v^2$", colour="C4", alpha=alpha, new=False, show=False
        )
        if theta_true is not None:
            plt.axhline(theta_true[-1], linestyle="--", color="C3")
    else:
        plot_sample_line(lp_samples, ylabel="$\\log(p(\\theta|x))$", colour="C4", alpha=alpha, new=False, show=False)
        if np.min(lp_samples) < -1e-3:
            plt.ylim(-1e3, np.max(lp_samples) + 100)

    save_and_show(**plot_kwargs)


def plot_sample_hist(
    y: np.ndarray,
    bins: int = 30,
    name: Optional[str] = None,
    y_true: Optional[float] = None,
    xlabel: Optional[str] = None,
    colour: Optional[str] = None,
    kde: bool = False,
    pdf_func: Optional[Callable[[np.ndarray], np.ndarray]] = None,
    new: bool = True,
    **plot_kwargs: Any,
) -> None:
    if new:
        new_fig("hist", (5, 4), **plot_kwargs)

    # padded x axis to prevent cutoff
    y_min = np.min(y)
    y_max = np.max(y)
    pad = (y_max - y_min) / 10
    x = np.linspace(y_min - pad, y_max + pad, 200)

    # plot KDE or histogram
    if kde:
        plt.plot(x, gaussian_kde(y).pdf(x), color=colour, label=name)
    else:
        plt.hist(y, bins=bins, density=True, color=colour, label=name)

    # add extras
    if pdf_func is not None:
        plt.plot(x, pdf_func(x), "k--")

    if y_true is not None:
        plt.axvline(y_true, linestyle="--", color="C3")

    if name is not None:
        plt.legend(loc="upper right")

    plt.xlabel(xlabel)
    plt.grid(True)
    save_and_show(**plot_kwargs)


def plot_sample_hists(
    theta_samples: np.ndarray,
    bins: int = 30,
    theta_true: Optional[np.ndarray] = None,
    kde: bool = False,
    pdf_funcs: Optional[List[Callable[[np.ndarray], np.ndarray]]] = None,
    converted_alpha: bool = False,
    **plot_kwargs: Any,
) -> None:
    p = theta_samples.shape[1] // 3

    if theta_true is None:
        theta_true = [None] * p * 3
    if pdf_funcs is None:
        pdf_funcs = [None] * p * 3

    # change label if given samples of alpha not alpha/2zeta
    labels = list(_I_HAT_NAMES)
    if converted_alpha:
        labels[2] = "\\hat{\\alpha_{#}}"

    fig = new_fig("hists", (12, p * 3), **plot_kwargs)

    for i, name in enumerate(labels):
        for j in range(p):
            index = i * p + j
            fig.add_subplot(p, 3, i + j * 3 + 1)
            plot_sample_hist(
                theta_samples[:, index],
                bins=bins,
                y_true=theta_true[index],
                name=f"${name.replace('#', str(j+1))}$",  # or xlabel
                colour=_COLOURS[j % len(_COLOURS)],
                kde=kde,
                pdf_func=pdf_funcs[index],
                new=False,
                show=False,
            )

    save_and_show(**plot_kwargs)


def plot_corner(
    theta_samples: np.ndarray,
    bins: int = 30,
    theta_true: Optional[np.ndarray] = None,
    show_mean: bool = True,
    bounds: Optional[np.ndarray] = None,
    group_by: Optional[str] = None,
    std_max: Optional[float] = None,
    percentile: Optional[Union[float, Tuple[float, float]]] = None,
    converted_alpha: bool = False,
    **plot_kwargs: Any,
) -> None:
    samples = theta_samples.copy()
    true = None if theta_true is None else theta_true.copy()
    p = samples.shape[1] // 3
    mean = np.mean(samples, axis=0)

    # change label if given samples of alpha not alpha/2zeta
    labels = list(_I_HAT_NAMES)
    if converted_alpha:
        labels[2] = "\\hat{\\alpha_{#}}"

    # set axis limits
    if bounds is None:
        bounds = np.vstack([np.min(samples, axis=0), np.max(samples, axis=0)])

        if std_max is not None:
            std = np.std(samples, axis=0)
            bounds[0] = np.maximum(mean - std_max * std, bounds[0])
            bounds[1] = np.minimum(mean + std_max * std, bounds[1])

        if percentile is not None:
            if isinstance(percentile, (int, float)):
                percentile = (percentile, 100 - percentile)

            bounds[0] = np.maximum(np.percentile(samples, percentile[0], axis=0), bounds[0])
            bounds[1] = np.minimum(np.percentile(samples, percentile[1], axis=0), bounds[1])

        bounds = bounds.T

    # common plot function for each grouping
    def _plot(
        _fig: plt.Figure,
        _samples: np.ndarray,
        _labels: List[str],
        _bounds: np.ndarray,
        _true: np.ndarray,
        _mean: np.ndarray,
    ) -> None:
        corner.corner(
            _samples,
            fig=_fig,
            labels=_labels,
            range=_bounds,
            bins=bins,
            show_titles=True,
            plot_datapoints=False,
            plot_contours=True,
        )

        if _true is not None:
            corner.overplot_lines(_fig, _true, linestyle="--", color="C3")
        if show_mean:
            corner.overplot_lines(_fig, _mean, linestyle="-", color="C2")

    if group_by is None:
        _plot(
            new_fig("corner", (12, 12), **plot_kwargs),
            samples,
            [f"${name.replace('#', str(j+1))}$" for name in labels for j in range(p)]
            + ["$\\log_{10} \\hat{\\sigma}_v^2$"],
            bounds,
            true,
            mean,
        )
        save_and_show(**plot_kwargs)

    elif group_by.lower() == "param":
        for i, name in enumerate(_NAMES):
            _plot(
                new_fig(f"corner_{name}", (8, 8), **plot_kwargs),
                samples[:, i * p : (i + 1) * p],
                [f"${labels[i].replace('#', str(j+1))}$" for j in range(p)],
                bounds[i * p : (i + 1) * p],
                true[i * p : (i + 1) * p] if true is not None else None,
                mean[i * p : (i + 1) * p],
            )
            file_path = plot_kwargs["file_path"] + f"_{i+1}" if "file_path" in plot_kwargs else None
            save_and_show(**dict(plot_kwargs, file_path=file_path))

    elif group_by.lower() == "mode":
        for i in range(p):
            _plot(
                new_fig(f"corner_{i+1}", (8, 8), **plot_kwargs),
                samples[:, i : 3 * p : p],
                [f"${name.replace('#', str(i+1))}$" for name in labels],
                bounds[i : 3 * p : p],
                true[i : 3 * p : p] if true is not None else None,
                mean[i : 3 * p : p],
            )
            file_path = plot_kwargs["file_path"] + f"_{i+1}" if "file_path" in plot_kwargs else None
            save_and_show(**dict(plot_kwargs, file_path=file_path))

    else:
        raise ValueError(f"'group_by' must be one of None, 'param' or 'mode', not '{group_by}'.")
