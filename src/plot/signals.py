"""File containing functions used to plot signals in the time and/or frequency domain."""

from typing import Any, Dict, List, Optional, Union

import numpy as np
from matplotlib import pyplot as plt

from src.plot.utils import iterate_args, new_fig, save_and_show, TAxes
from src.utils import MeasureType, to_db

_Y_LABELS = {MeasureType.dis: "Displacement", MeasureType.vel: "Velocity", MeasureType.acc: "Acceleration"}


def plot_td(
    t: TAxes,
    y: TAxes,
    names: Optional[Union[List[str], str]] = None,
    measure: Optional[MeasureType] = None,
    new: bool = True,
    **plot_kwargs: Any,
) -> None:
    if new:
        new_fig("FD", (7, 4), **plot_kwargs)

    for _t, _y, _name in iterate_args(t, y, names):
        plt.plot(_t, _y, label=_name)

    if names is not None:
        plt.legend()

    plt.xlabel("Time (s)")
    plt.ylabel(_Y_LABELS.get(measure, "Amplitude"))
    plt.grid(True)

    save_and_show(**plot_kwargs)


def plot_td_sims(
    t: np.ndarray,
    y_obs: np.ndarray,
    y_sims: List[np.ndarray],
    names: Optional[List[str]] = None,
    alpha: float = 0.1,
    measure: Optional[MeasureType] = None,
    new: bool = True,
    **plot_kwargs: Any,
) -> None:
    if new:
        new_fig("TD_sims", (10, 5), **plot_kwargs)

    for j, y_sim in enumerate(y_sims):
        plt.plot(t, y_sim, "C1", alpha=alpha, label=names[1] if j == 0 and names is not None else None)

    plt.plot(t, y_obs, "C0", label=names[0] if names is not None else None)

    if names is not None:
        plt.legend()

    plt.xlabel("Time (s)")
    plt.ylabel(_Y_LABELS.get(measure, "Amplitude"))
    plt.grid(True)

    save_and_show(**plot_kwargs)


def plot_fd(
    f: TAxes,
    y: TAxes,
    names: Optional[Union[List[str], str]] = None,
    hertz: bool = False,
    convert_db: bool = True,
    fmax: float = np.inf,
    measure: Optional[MeasureType] = None,
    new: bool = True,
    **plot_kwargs: Any,
) -> None:
    if new:
        new_fig("FD", (7, 4), **plot_kwargs)

    for _f, _y, _name in iterate_args(f, y, names):
        mask = _f <= fmax
        if convert_db:
            _y = to_db(_y)
        plt.plot(_f[mask], _y[mask], label=_name)

    if names is not None:
        plt.legend()

    plt.xlabel("Frequency (Hz)" if hertz else "Frequency (rad/s)")
    plt.ylabel(_Y_LABELS.get(measure, "Amplitude") + (" (dB)" if convert_db else ""))
    plt.grid(True)

    save_and_show(**plot_kwargs)


def plot_fd_sims(
    f: np.ndarray,
    y_obs: np.ndarray,
    y_sims: List[np.ndarray],
    names: Optional[List[str]] = None,
    alpha: float = 0.1,
    hertz: bool = False,
    convert_db: bool = True,
    fmax: float = np.inf,
    measure: Optional[MeasureType] = None,
    new: bool = True,
    **plot_kwargs: Any,
) -> None:
    if new:
        new_fig("FD_sims", (10, 5), **plot_kwargs)

    mask = f <= fmax
    for i, y_sim in enumerate(y_sims):
        if convert_db:
            y_sim = to_db(y_sim)
        plt.plot(f[mask], y_sim[mask], "C1", alpha=alpha, label=names[1] if i == 0 and names is not None else None)

    if convert_db:
        y_obs = to_db(y_obs)
    plt.plot(f[mask], y_obs[mask], "C0", label=names[0] if names is not None else None)

    if names is not None:
        plt.legend()

    plt.xlabel("Frequency (Hz)" if hertz else "Frequency (rad/s)")
    plt.ylabel(_Y_LABELS.get(measure, "Amplitude") + (" (dB)" if convert_db else ""))
    plt.grid(True)

    save_and_show(**plot_kwargs)


def plot_td_fd(
    t: TAxes,
    y_td: TAxes,
    f: TAxes,
    y_fd: TAxes,
    names: Optional[Union[List[str], str]] = None,
    hertz: bool = False,
    convert_db: bool = True,
    fmax: float = np.inf,
    measure: Optional[MeasureType] = None,
    **plot_kwargs: Any,
) -> None:
    fig = new_fig("TD_FD", (12, 4), **plot_kwargs)

    fig.add_subplot(1, 2, 1)
    plot_td(t, y_td, names, measure=measure, new=False, show=False)

    fig.add_subplot(1, 2, 2)
    plot_fd(f, y_fd, names, hertz, convert_db, fmax, measure=measure, new=False, show=False)

    save_and_show(**plot_kwargs)


def plot_omega_bounds(
    f: np.ndarray,
    y: np.ndarray,
    omega_center: np.ndarray,
    omega_hw: np.ndarray,
    y_locs: Union[np.ndarray, List[float]],
    fd_kwargs: Dict[str, Any] = None,
    **plot_kwargs: Any,
) -> None:
    new_fig("omega_bounds", (7, 4), **plot_kwargs)
    plot_fd(f, y, **(fd_kwargs if fd_kwargs is not None else {}), show=False, new=False)
    plt.errorbar(omega_center, y_locs, linewidth=0, elinewidth=1.5, xerr=omega_hw, color="k", capsize=4)
    save_and_show(**plot_kwargs)


def plot_alpha_bounds(
    f: np.ndarray,
    y: np.ndarray,
    a2z_center: np.ndarray,
    a2z_hw: np.ndarray,
    x_locs: Union[np.ndarray, List[float]],
    fd_kwargs: Dict[str, Any] = None,
    **plot_kwargs: Any,
) -> None:
    new_fig("alpha_bounds", (7, 4), **plot_kwargs)
    plot_fd(f, y, **(fd_kwargs if fd_kwargs is not None else {}), show=False, new=False)

    y_min = plt.gca().get_ylim()[0]
    if fd_kwargs.get("convert_db", True):
        # TODO will cause bug if y_min should change
        a2z_min = np.nan_to_num(to_db(a2z_center - a2z_hw), neginf=y_min - 0.1)
        a2z_max = to_db(a2z_center + a2z_hw)
        a2z_center = to_db(a2z_center)
        a2z_hw = np.vstack((a2z_center - a2z_min, a2z_max - a2z_center))

    plt.errorbar(x_locs, a2z_center, linewidth=0, elinewidth=1.5, yerr=a2z_hw, color="k", capsize=4)

    if fd_kwargs.get("convert_db", True):
        plt.ylim(y_min)

    save_and_show(**plot_kwargs)
