"""File containing the `EMCEESampler` class."""

import time
from multiprocessing import Pool
from typing import Any, Optional, Tuple, TYPE_CHECKING

import emcee
import numpy as np

from src.sample.samplers import MultiChainSampler

if TYPE_CHECKING:
    from src.sample.distributions import LogPosterior


def _init_backend(hdf_file_path: str, file_mode: str, shape: Tuple[int, int]):
    if file_mode not in {"r", "x", "w"}:
        raise ValueError("file_mode must be one of 'r', 'x' or 'w'.")

    backend = emcee.backends.HDFBackend(hdf_file_path)
    if backend.initialized:  # file already exists
        if file_mode == "r":
            backend.read_only = True
        elif file_mode == "w":
            print(f"Overwriting '{hdf_file_path}'.")
            backend.reset(*shape)
        else:
            raise FileExistsError(f"File at '{hdf_file_path}' already exists.")
    else:
        if file_mode == "r":
            raise FileNotFoundError(f"Cannot find file '{hdf_file_path}'.")

    return backend


class EMCEESampler(MultiChainSampler):
    """Class defining an affine-invariant ensemble sampling algorithm [1]_ [2]_.

    References
    ----------
    .. [1] Goodman & Weare (2010) https://cims.nyu.edu/~weare/papers/d13.pdf
    .. [2] Foreman-Mackey, Hogg, Lang & Goodman (2012) https://arxiv.org/abs/1202.3665
    """

    def __init__(
        self,
        log_post: "LogPosterior",
        nit: int,
        initial_thetas: np.ndarray,
        *,
        pool: Optional[Pool] = None,
        hdf_file_path: Optional[str] = None,
        file_mode: str = "x",
    ):
        """Initialise an `EMCEESampler` instance.

        Parameters
        ----------
        log_post
            `Distributions` instance, defining the log posterior of a parameter `theta`.
        nit
            Number of samples to draw.
        initial_thetas
            `num_walkers` x `ndim` matrix of initial values for `theta`. Note that
             `nit` % `num_walkers` == 0 is required.
        pool
            multiprocessing.Pool for multiprocessing.
        hdf_file_path
            If specified, will store samples in HDF format at this location instead
            of in memory.
        file_mode
            How to open the given hdf file. Must be one of: "r" (read only), "x"
            (create, raises if exists), "w" (create / overwrite).
        """
        super().__init__(log_post, initial_thetas.shape[1], nit, initial_thetas.shape[0])
        self.initial_thetas = initial_thetas

        # store in memory
        if hdf_file_path is None:
            if file_mode == "r":
                raise ValueError("Must specify file path for mode 'r'.")
            backend = None

        # store in file
        else:
            if not hdf_file_path.endswith(".hdf"):
                hdf_file_path += ".hdf"
            backend = _init_backend(hdf_file_path, file_mode, (self.num_walkers, self.ndim))

        self.emcee = emcee.EnsembleSampler(
            nwalkers=self.num_walkers, ndim=self.ndim, log_prob_fn=self.log_post, pool=pool, backend=backend
        )

    @property
    def backend(self):
        """The backend of the emcee sampler."""
        return self.emcee.backend

    @property
    def acc_frac(self):
        return self.emcee.acceptance_fraction[np.r_[self.chain]]

    @staticmethod
    def _get_samples(get_method, chain, burn_in, thin):
        samples = get_method(flat=False, thin=thin, discard=burn_in)[:, chain]
        if not isinstance(chain, int):
            samples = np.swapaxes(samples, 0, 1)
        return samples

    @property
    def samples(self):
        return self._get_samples(self.backend.get_chain, self.chain, self.burn_in, self.thin)

    @property
    def lp_samples(self):
        return self._get_samples(self.backend.get_log_prob, self.chain, self.burn_in, self.thin)

    def auto_burn(self, lp_cutoff: Optional[int] = None, *, burn_increment: int = 20):
        """Automatically select and burn the chains to maximise the number of converged samples.

        Parameters
        ----------
        lp_cutoff
            Minimum log posterior value for a chain to be considered converged. Default is maximum - 40.
        burn_increment
            How much to increment the burn in period by in each test. Default is 20.
        """
        self.burn_in = 0
        self.thin = 1
        self.chain = None

        if lp_cutoff is None:
            lp_cutoff = np.max(self.lp_samples) - 40

        best = (-1, None, 0)  # (no. samples, chain, burn-in)
        for _ in range(self.num_steps // burn_increment - 1):
            chain = tuple(np.argwhere(np.mean(self.lp_samples[:, 0 : burn_increment // 4], axis=1) > lp_cutoff)[:, 0])
            num_samples = (self.num_steps - self.burn_in) * len(chain)
            if num_samples > best[0]:
                best = (num_samples, chain, self.burn_in)
            self.burn_in += burn_increment
        _, self.chain, self.burn_in = best

    def run(self, *, seed: Optional[int] = None, show_progress: bool = True, **kwargs: Any):
        if getattr(self.backend, "read_only", False):
            raise FileExistsError("Cannot run read only sampler.")

        if seed is not None:
            self.emcee._random.seed(seed)  # TODO

        start = time.time()
        self.emcee.run_mcmc(self.initial_thetas, self.num_steps, progress=show_progress, **kwargs)
        self.elapsed = time.time() - start

        if hasattr(self.backend, "read_only"):
            self.backend.read_only = True
