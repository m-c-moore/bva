"""File containing functions to approximate the model evidence."""

from typing import Callable, List, Optional, Tuple, Union

import numpy as np
import scipy.special as sp
from scipy.linalg import fractional_matrix_power
from tqdm import tqdm

try:
    import faiss
except ImportError:
    pass


def laplace_approx(
    samples: np.ndarray, log_pdf: Callable[[np.ndarray], float], mean: Optional[np.ndarray] = None
) -> Tuple[np.ndarray, np.ndarray, float]:
    """Return the Laplace approximation mean, covariance and log model evidence estimate.

    Parameters
    ----------
    samples
        Samples of theta.
    log_pdf
        Log posterior function.
    mean
        Optionally set the mean (e.g. to MAP estimate).
    """
    if mean is None:
        mean = np.mean(samples, axis=0)
    cov = np.cov(samples, rowvar=False)

    log_marginal_density = (
        0.5 * samples.shape[1] * np.log(2 * np.pi)
        + np.linalg.slogdet(fractional_matrix_power(cov, 0.5))[1]
        + log_pdf(mean)
    )

    return mean, cov, log_marginal_density


def _get_cov_stats(samples: np.ndarray) -> Tuple[np.ndarray, float, np.ndarray, np.ndarray]:
    cov = np.cov(samples.T)
    e_val, e_vec = np.linalg.eig(cov)

    if (e_val < 0).any():
        print("WARN: Some E-values of the covariance matrix are negative and/or complex")
        jac = 1
    else:
        jac = np.sqrt(np.linalg.det(cov))

    return cov, jac, e_vec, e_val


def mle_evidence(
    samples: np.ndarray, lp_samples: np.ndarray, chunks: Union[int, List[int]] = 10_000, k: int = 1
) -> np.ndarray:
    """Return the maximum likelihood estimated for the model evidence.

    Based on the Marginal Likelihoods from Monte Carlo Markov Chains algorithm [1]_ [2]_.

    Parameters
    ----------
    samples
        Samples of theta.
    lp_samples
        Samples of the log posterior.
    chunks
        Chunk size. Lower decreases accuracy but increases speed. Default is 10,000.
    k
        Number of nearest neighbours to use. Recommended is 1.

    References
    ----------
    .. [1] Heavens et. al. (2017) https://arxiv.org/abs/1704.03472
    .. [2] https://github.com/yabebalFantaye/MCEvidence
    """
    if k < 1:
        raise ValueError("k must be >= 1")
    N, ndim = samples.shape

    if isinstance(chunks, int):
        if chunks > N:
            chunks = [N]
        else:
            chunks = [chunks] * (N // chunks)

    MLE = np.zeros(len(chunks))

    for i, chunk_size in enumerate(tqdm(chunks)):
        # chunk samples
        _samples = samples[i * chunk_size : (i + 1) * chunk_size, :]
        _lp_samples = lp_samples[i * chunk_size : (i + 1) * chunk_size]

        # get covariance stats
        cov, jac, e_vec, e_val = _get_cov_stats(_samples)

        # diagonalise samples to have unit variance. Change type for faiss.
        _samples = np.divide(_samples @ e_vec, np.sqrt(e_val)).astype("float32")

        # kth nearest neighbour distances
        neighbours = faiss.IndexFlatL2(ndim)
        neighbours.add(_samples)
        distances, _ = neighbours.search(_samples, k)

        # posterior for a
        volume = np.power(np.pi, ndim / 2) / sp.gamma(1 + ndim / 2) * np.power(distances[:, k - 1].T, ndim)

        # MAP estimate for a
        lp_max = np.max(_lp_samples)
        _lp_samples -= lp_max  # to avoid underflow
        a_map = (volume @ np.exp(_lp_samples)) / (len(_samples) * k + 1.0)

        # MLE estimate for evidence
        MLE[i] = np.log(a_map * jac) + lp_max

    return MLE
