"""File containing the `LogPDF` class and its subclasses."""

import warnings
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Callable, List, Optional, TYPE_CHECKING, Union

import numpy as np
import scipy.stats as stats

if TYPE_CHECKING:
    from src.simulate import Simulator
    from src.sample.noise_models import NoiseModel


@dataclass
class LogPDF(ABC):
    """Abstract base class from which probability density function classes are extended."""

    @abstractmethod
    def __call__(self, theta: np.ndarray) -> float:
        """Return the log probability of a set of parameters."""
        pass


@dataclass
class LogPrior(LogPDF, ABC):
    """Abstract base class from which prior distribution classes are extended."""

    def __post_init__(self) -> None:
        self._dist: Optional[stats.rv_frozen] = None

    def __call__(self, theta):
        return np.sum(self._dist.logpdf(theta))

    @property
    def num_params(self):
        """Dimensionality of the distribution."""
        return len(self._dist.args[0])

    @property
    @abstractmethod
    def marg_pdfs(self) -> List[Callable]:
        """List of marginal probability distributions (not in the log domain)."""
        pass

    def sample(self, n: int, *, seed: Optional[int] = None):
        """Return `n` independent samples from the distribution."""
        return self._dist.rvs((n, self.num_params), seed)


@dataclass
class GaussLogPrior(LogPrior):
    """Class defining a Gaussian prior distribution."""

    mu: np.ndarray
    sd: np.ndarray

    def __post_init__(self) -> None:
        if not all(self.sd >= 0):
            warnings.warn("Negative standard deviations converted to absolute value.")
            self.sd = np.abs(self.sd)
        self._dist = stats.norm(self.mu, self.sd)

    @property
    def marg_pdfs(self):
        return [stats.norm(self.mu[i], self.sd[i]).pdf for i in range(self.num_params)]


@dataclass
class UniformLogPrior(LogPrior):
    """Class defining a uniform prior distribution."""

    lb: np.ndarray
    ub: np.ndarray

    def __post_init__(self) -> None:
        if not all(self.lb < self.ub):
            bad = np.argwhere(self.lb >= self.ub).T[0]
            raise ValueError(f"Lower bounds must be < upper bounds: check elements {bad}")
        self._dist = stats.uniform(self.lb, self.ub - self.lb)

    @property
    def marg_pdfs(self):
        return [stats.uniform(self.lb[i], self.ub[i] - self.lb[i]).pdf for i in range(self.num_params)]


@dataclass
class LogObsLik(LogPDF):
    """Class defining a likelihood distribution based on comparing the observations to simulations."""

    observations: np.ndarray = field(repr=False)
    simulator: "Simulator"
    noise_model: "NoiseModel"

    def __call__(self, theta):
        lp = self.noise_model(self.observations, self.simulator, theta)
        return -np.inf if np.isnan(lp) else lp


@dataclass
class LogPosterior(LogPDF):
    """Class defining a posterior distribution based on the combination of a prior and likelihood."""

    log_prior: Union[UniformLogPrior, GaussLogPrior]
    log_likelihood: LogObsLik

    def __call__(self, theta):
        return self.log_prior(theta) + self.log_likelihood(theta)
