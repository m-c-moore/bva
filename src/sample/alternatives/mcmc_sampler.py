"""File containing the abstract `MCMCSampler` and `MultiChainMCMCSampler` classes."""

from abc import ABC
from typing import Optional, Type, TYPE_CHECKING

import numpy as np
from tqdm import tqdm

from src.sample.samplers import MultiChainSampler, Sampler

if TYPE_CHECKING:
    from src.sample.distributions import LogPosterior


class MCMCSampler(Sampler, ABC):
    """Abstract class from which MCMC sampling algorithms are extended."""

    def __init__(self, log_post: "LogPosterior", nit: int, initial_theta: np.ndarray, **kwargs):
        """Initialise a `MCMCSampler` instance.

        Parameters
        ----------
        log_post
            Function that returns the log probability density function of parameter vector.
        nit
            Number of samples to draw.
        initial_theta
            Starting point of the algorithm.
        """
        super().__init__(log_post, initial_theta.shape[0], nit)
        self.initial_theta = initial_theta
        self._accepted = 0

        self._samples = np.zeros((self.nit, self.ndim))
        self._lp_samples = np.zeros(self.nit)

    @property
    def samples(self) -> np.ndarray:
        return self._samples[self.burn_in :: self.thin]

    @property
    def lp_samples(self) -> np.ndarray:
        return self._lp_samples[self.burn_in :: self.thin]

    @property
    def acc_frac(self):
        return self._accepted / self.nit

    def _mh_step(self, i: int, log_u: float, step: np.ndarray):
        self._samples[i] = self._samples[i - 1] + step
        self._lp_samples[i] = self.log_post(self._samples[i])

        if log_u <= self._lp_samples[i] - self._lp_samples[i - 1]:
            self._accepted += 1
        else:
            self._samples[i] = self._samples[i - 1]
            self._lp_samples[i] = self._lp_samples[i - 1]


class MultiChainMCMCSampler(MultiChainSampler):
    """Class defining a collection of independent `MCMCSampler` instances."""

    def __init__(
        self, log_post: "LogPosterior", nit: int, initial_thetas: np.ndarray, walker_class: Type[MCMCSampler], **kwargs
    ):
        """Initialise a `MCMCSampler` instance.

        Parameters
        ----------
        log_post
            Function that returns the log probability density function of parameter vector.
        nit
            Number of samples to draw.
        initial_thetas
            `num_walkers` x `ndim` matrix of initial values for `theta`. Note that
             `nit` % `num_walkers` == 0 is required.
        walker_class
            Class of the individual walkers.
        kwargs
            Passed to `walker_class.__init__()`.
        """
        super().__init__(log_post, initial_thetas.shape[1], nit, initial_thetas.shape[0])
        self.initial_thetas = initial_thetas
        self.walkers = [walker_class(self.log_post, self.num_steps, it, **kwargs) for it in self.initial_thetas]

    @MultiChainSampler.burn_in.setter
    def burn_in(self, value: int):
        for w in self.walkers:
            w.burn_in = value

    @MultiChainSampler.thin.setter
    def thin(self, value: int):
        for w in self.walkers:
            w.thin = value

    def _get_walker_attr(self, attr: str):
        # fetch samples each time to allow individual burn and thin
        if isinstance(self.chain, int):
            return getattr(self.walkers[self.chain], attr)
        return np.array([getattr(self.walkers[i], attr) for i in tuple(self.chain)])

    @property
    def samples(self) -> np.ndarray:
        return self._get_walker_attr("samples")

    @property
    def lp_samples(self) -> np.ndarray:
        return self._get_walker_attr("lp_samples")

    @property
    def acc_frac(self):
        return self._get_walker_attr("acc_frac")

    def run(self, *, seed: Optional[int] = None, show_progress: bool = True, **kwargs):
        for i, walker in enumerate(tqdm(self.walkers, disable=not show_progress)):
            walker.run(seed=None if seed is None else seed + i, show_progress=False)
