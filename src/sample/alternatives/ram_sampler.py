"""File containing the `RAMSampler` class."""

from typing import Optional, TYPE_CHECKING

import numpy as np
from numba import jit
from tqdm import tqdm

from src.sample.alternatives.mcmc_sampler import MCMCSampler

if TYPE_CHECKING:
    from src.sample.distributions import LogPosterior


@jit(nopython=True)
def _get_updated_covariance(
    i: int, z: np.ndarray, L: np.ndarray, accepted: float, gamma: float, target_frac_acc: float, D: int
):
    frac_acc = accepted / (i + 1)
    eta = min(1.0, D * (i + 1) ** gamma)
    M = np.eye(D) + eta * (frac_acc - target_frac_acc) * np.outer(z, z) / (z @ z)

    cov = L @ M @ L.T
    L = np.linalg.cholesky(np.maximum(cov, cov.T))
    return cov, L


class RAMSampler(MCMCSampler):
    """Class defining a Robust Adaptive Metropolis sampling algorithm [1]_.

    References
    ----------
    .. [1] Vihola (2011) https://arxiv.org/abs/1011.4381
    """

    def __init__(
        self,
        log_post: "LogPosterior",
        nit: int,
        initial_theta: np.ndarray,
        initial_cov: np.ndarray,
        target_frac_acc: float = 0.234,  # recommended
        gamma: float = 2 / 3,  # recommended
    ):
        super().__init__(log_post, nit, initial_theta)

        if not 0 < target_frac_acc < 1:
            raise ValueError("Target acceptance fraction must be between 0 and 1")

        if not 0.5 < gamma <= 1:
            raise ValueError("Gamma must be between 0.5 and 1")

        self.gamma = gamma
        self.target_frac_acc = target_frac_acc

        if initial_cov.ndim == 1:
            self.cov = np.diag(initial_cov)
        else:
            self.cov = initial_cov.copy()

        self.cov_norm_samples = np.zeros(self.nit)
        self.cov_norm_samples[0] = np.linalg.norm(self.cov)

    def _update_covariance(self, i: int, z: np.ndarray, L: np.ndarray):
        self.cov[:], L[:] = _get_updated_covariance(i, z, L, self._accepted, self.gamma, self.target_frac_acc, len(z))
        self.cov_norm_samples[i] = np.linalg.norm(self.cov)

    def run(self, *, seed: Optional[int] = None, show_progress: bool = True, **kwargs):
        """Run the sampler."""
        if seed is not None:
            np.random.seed(seed)

        # pre-generate random numbers
        z_arr = np.random.normal(size=(self.nit, self.ndim))
        log_u_arr = np.log(np.random.rand(self.nit))

        # will be overwritten, just helps with loop
        self._samples[-1] = self.initial_theta
        self._lp_samples[-1] = self.log_post(self.initial_theta)

        L = np.linalg.cholesky(self.cov)

        for i in tqdm(range(self.nit), disable=not show_progress):
            self._mh_step(i, log_u_arr[i], L @ z_arr[i])
            self._update_covariance(i, z_arr[i], L)
