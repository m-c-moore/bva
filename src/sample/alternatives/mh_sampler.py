"""File containing the `MHSampler` class."""

from typing import Optional, TYPE_CHECKING

import numpy as np
from tqdm import tqdm

from src.sample.alternatives.mcmc_sampler import MCMCSampler

if TYPE_CHECKING:
    from src.sample.distributions import LogPosterior


class MHSampler(MCMCSampler):
    """Class defining the Metropolis-Hastings sampling algorithm [1]_.

    References
    ----------
    .. [1] Hastings (1970) https://www.jstor.org/stable/2334940
    """

    def __init__(self, log_post: "LogPosterior", nit: int, initial_theta: np.ndarray, step_cov: np.ndarray):
        super().__init__(log_post, nit, initial_theta)
        if step_cov.ndim == 1:
            self.step_cov = np.diag(step_cov)
        else:
            self.step_cov = step_cov.copy()

    def run(self, *, seed: Optional[int] = None, show_progress: bool = True, **kwargs):
        if seed is not None:
            np.random.seed(seed)

        # pre-generate random numbers
        steps = np.random.multivariate_normal(np.zeros(self.ndim), self.step_cov, self.nit)
        log_u_arr = np.log(np.random.rand(self.nit))

        # will be overwritten, just helps with loop
        self._samples[-1] = self.initial_theta
        self._lp_samples[-1] = self.log_post(self.initial_theta)

        # run sampler
        for i in tqdm(range(self.nit), disable=not show_progress):
            self._mh_step(i, log_u_arr[i], steps[i])
