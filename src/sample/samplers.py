"""File containing the abstract `Sampler` and `MultiChainSampler` classes."""

from abc import ABC, abstractmethod
from typing import Any, Optional, Tuple, TYPE_CHECKING, Union

import numpy as np
from scipy.optimize import minimize

from src.sample.evidence import laplace_approx, mle_evidence
from src.simulate.converters import Converter

if TYPE_CHECKING:
    from src.sample.distributions import LogPosterior


class Sampler(ABC):
    """Abstract base class from which sampling algorithms are extended."""

    def __init__(self, log_post: "LogPosterior", ndim: int, nit: int):
        """Initialise a `Sampler` instance.

        Parameters
        ----------
        log_post
            Function that returns the log probability density function of parameter vector.
        ndim
            Number of dimensions of the parameter vector
        nit
            Number of samples to draw.
        """
        self.log_post = log_post
        self.ndim = ndim
        self.nit = nit

        self._burn_in = 0
        self._thin = 1

        self.elapsed = -1.0

    @property
    def burn_in(self):
        """Burn in period."""
        return self._burn_in

    @burn_in.setter
    def burn_in(self, value: int):
        if value >= self.nit:
            raise ValueError("Burn-in period cannot be larger than the number of samples")
        self._burn_in = value

    @property
    def thin(self):
        """Thinning interval."""
        return self._thin

    @thin.setter
    def thin(self, value: int):
        if value >= self.nit:
            raise ValueError("Thinning interval cannot be larger than the number of samples")
        self._thin = value

    @property
    @abstractmethod
    def samples(self) -> np.ndarray:
        """Burned-in and thinned samples of `theta`."""
        pass

    @property
    @abstractmethod
    def lp_samples(self) -> np.ndarray:
        """Burned-in and thinned samples of `log_post`."""
        pass

    @property
    def theta_max(self) -> np.ndarray:
        """The sample corresponding to the maximum posterior sample.

        Note that these samples may be burned or thinned.
        """
        return self.samples[self.lp_samples.argmax()]

    @property
    def theta_max_all(self):
        """The sample corresponding to the maximum posterior sample.

        Includes samples that would otherwise be burned or thinned.
        """
        burn, thin = self.burn_in, self.thin
        self.burn_in, self.thin = 0, 1
        theta_max = self.theta_max
        self.burn_in, self.thin = burn, thin
        return theta_max

    @property
    def theta_hat(self) -> np.ndarray:
        """The mean of the burned-in and thinned samples of `theta`."""
        return np.mean(self.samples, axis=0)

    @property
    def theta_hat_sd(self) -> np.ndarray:
        """The standard deviation of the burned-in and thinned samples of `theta`."""
        return np.std(self.samples, axis=0)

    @property
    @abstractmethod
    def acc_frac(self):
        """Fraction of accepted samples."""
        pass

    def theta_opt(self, guess: np.ndarray = None, max_sim: int = 5000, verbose: bool = True) -> np.ndarray:
        """Return the value of `theta` obtained by maximising the log posterior."""
        if guess is None:
            guess = self.theta_max_all
            if verbose:
                print("No guess provided, using the MAP sample.")

        def func_to_min(theta):
            return -self.log_post(theta)

        res = minimize(func_to_min, guess, method="L-BFGS-B", options=dict(maxfun=max_sim))
        if verbose:
            print(res)
        return res.x

    def laplace_approx(self, *args):
        """Return the Laplace approximation of the model evidence."""
        return laplace_approx(self.samples, self.log_post, *args)

    def bayesian_information_criterion(self, N: int):
        """Return the BIC value."""
        return self.ndim * np.log(N) - 2 * self.log_post.log_likelihood(self.theta_max_all)

    def mle_evidence(self, **kwargs: Any):
        """Return the maximum likelihood estimate for the model evidence."""
        return mle_evidence(self.samples, self.lp_samples, **kwargs)

    @abstractmethod
    def run(self, *, seed: Optional[int] = None, show_progress: bool = True, **kwargs):
        """Run the sampler."""
        pass


class MultiChainSampler(Sampler, ABC):
    """Abstract base class from which multi-chain sampling algorithms are extended."""

    def __init__(self, log_post: "LogPosterior", ndim: int, nit: int, num_walkers: int):
        """

        Parameters
        ----------
        log_post
            Function that returns the log probability density function of parameter vector.
        ndim
            Number of dimensions of the parameter vector
        nit
            Number of samples to draw.
        num_walkers
            Number of chains / walkers.
        """
        super().__init__(log_post, ndim, nit)

        if nit % num_walkers != 0:
            raise ValueError("nit % num_walkers must equal 0")

        self.num_walkers = num_walkers
        self.num_steps = self.nit // self.num_walkers

        # only consider samples from specified chain(s)
        self._chain: Union[Tuple[int, ...], int] = tuple(range(self.num_walkers))

    @Sampler.burn_in.setter
    def burn_in(self, value: int):
        if value >= self.num_steps:
            raise ValueError("Burn-in period cannot be larger than the number of samples per chain")
        self._burn_in = value

    @Sampler.thin.setter
    def thin(self, value: int):
        if value >= self.num_steps:
            raise ValueError("Thinning interval cannot be larger than the number of samples per chain")
        self._thin = value

    @property
    def chain(self):
        """Indices of the chains to include samples from."""
        return self._chain

    @chain.setter
    def chain(self, value: Optional[Union[Tuple[int, ...], int]]):
        if value is None:
            value = tuple(range(self.num_walkers))
        elif isinstance(value, tuple):
            for v in value:
                if v >= self.num_walkers:
                    raise IndexError(f"Chain number {v} out of range for {self.num_walkers} walkers")
        elif isinstance(value, int):
            if value >= self.num_walkers:
                raise IndexError(f"Chain number {value} out of range for {self.num_walkers} walkers")
        else:
            raise TypeError(f"Invalid chain type '{type(value)}'")

        self._chain = value

    @property
    def flat_samples(self) -> np.ndarray:
        """Flattened array of samples across the chains."""
        samples = self.samples
        shape = samples.shape
        return np.reshape(samples, (shape[0] * shape[1], shape[2]))

    @property
    def flat_lp_samples(self) -> np.ndarray:
        """Flattened array of log posterior samples across the chains."""
        lp_samples = self.lp_samples
        shape = lp_samples.shape
        return np.reshape(lp_samples, (shape[0] * shape[1]))

    @property
    def flat_acc_frac(self) -> float:
        """Average acceptance fraction across the chains."""
        return np.mean(self.acc_frac)

    def flat_converted_samples(self, converter: Converter) -> np.ndarray:
        """Return the converted flattened samples."""
        return converter.from_theta(self.flat_samples)

    @property
    def theta_max(self) -> np.ndarray:
        """The sample corresponding to the maximum posterior sample.

        Note that these samples may be burned or thinned.
        """
        return self.flat_samples[self.flat_lp_samples.argmax()]

    @property
    def theta_max_all(self):
        """The sample corresponding to the maximum posterior sample across all chains.

        Includes samples that would otherwise be burned or thinned.
        """
        chain = self.chain
        self.chain = None
        theta_max = super().theta_max_all
        self.chain = chain
        return theta_max

    def laplace_approx(self, *args):
        """Return the Laplace approximation of the model evidence."""
        return laplace_approx(self.flat_samples, self.log_post, *args)

    def mle_evidence(self, **kwargs: Any):
        """Return the maximum likelihood estimate for the model evidence."""
        return mle_evidence(self.flat_samples, self.flat_lp_samples, **kwargs)
