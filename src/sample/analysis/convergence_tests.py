"""File containing functions to test for convergence."""

import numpy as np


def gelman_rubin_test(samples: np.ndarray) -> np.ndarray:
    """Return an array of the Gelman-Rubin potential scale reduction factors for each parameter [1]_.

    This code is based on the PyMC package [2]_.

    References
    ----------
    .. [1] Gelman and Rubin (1992) https://doi.org/10.1214/ss/1177011136
    .. [2] https://github.com/pymc-devs/pymc/blob/master/pymc/diagnostics.py
    """
    m, n, r = np.shape(samples)
    R = np.zeros(r)

    for i in range(r):
        x = samples[:, :, i]
        B_over_n = np.sum((np.mean(x, 1) - np.mean(x)) ** 2) / (m - 1)
        W = np.sum([(x[j] - x_bar) ** 2 for j, x_bar in enumerate(np.mean(x, 1))]) / (m * (n - 1))

        s2 = W * (n - 1) / n + B_over_n
        V = s2 + B_over_n / m
        R[i] = (V / W) ** 0.5

    return R
