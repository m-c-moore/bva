"""File containing functions to test for multivariate normality."""

from typing import Tuple

import numpy as np
from scipy.stats import lognorm


def henze_zirkler_test(samples: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Return the Henze-Zirkler test statistic and p-value [1]_.

    This code is based on the Pingouin package [2]_.

    References
    ----------
    .. [1] Henze, N., & Zirkler, B. (1990) https://doi.org/10.1080/03610929008830400
    .. [2] https://github.com/raphaelvallat/pingouin/blob/master/pingouin/multivariate.py
    """
    n, p = samples.shape

    # covariance matrix
    S = np.cov(samples, rowvar=False, bias=True)
    S_inv = np.linalg.pinv(S)
    diff = samples - np.mean(samples, axis=0)

    # squared-Mahalanobis distances
    Dj = np.diag(np.linalg.multi_dot([diff, S_inv, diff.T]))
    Y = np.linalg.multi_dot([samples, S_inv, samples.T])
    Djk = -2 * Y.T + np.repeat(np.diag(Y.T), n).reshape(n, -1) + np.tile(np.diag(Y.T), (n, 1))

    # smoothing parameter
    b = 1 / (np.sqrt(2)) * ((2 * p + 1) / 4) ** (1 / (p + 4)) * (n ** (1 / (p + 4)))

    # is matrix full-rank?
    if np.linalg.matrix_rank(S) == p:
        hz = n * (
            1 / (n ** 2) * np.sum(np.sum(np.exp(-(b ** 2) / 2 * Djk)))
            - 2 * ((1 + (b ** 2)) ** (-p / 2)) * (1 / n) * (np.sum(np.exp(-((b ** 2) / (2 * (1 + (b ** 2)))) * Dj)))
            + ((1 + (2 * (b ** 2))) ** (-p / 2))
        )
    else:
        hz = n * 4

    wb = (1 + b ** 2) * (1 + 3 * b ** 2)
    a = 1 + 2 * b ** 2

    # mean and variance
    mu = 1 - a ** (-p / 2) * (1 + p * b ** 2 / a + (p * (p + 2) * (b ** 4)) / (2 * a ** 2))
    si2 = (
        2 * (1 + 4 * b ** 2) ** (-p / 2)
        + 2 * a ** (-p) * (1 + (2 * p * b ** 4) / a ** 2 + (3 * p * (p + 2) * b ** 8) / (4 * a ** 4))
        - 4 * wb ** (-p / 2) * (1 + (3 * p * b ** 4) / (2 * wb) + (p * (p + 2) * b ** 8) / (2 * wb ** 2))
    )

    # log-normal mean and variance
    pmu = np.log(np.sqrt(mu ** 4 / (si2 + mu ** 2)))
    psi = np.sqrt(np.log((si2 + mu ** 2) / mu ** 2))
    pval = lognorm.sf(hz, psi, scale=np.exp(pmu))

    return hz, pval
