"""File containing classes defining noise models used to calculate the likelihoods."""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import TYPE_CHECKING

import numpy as np

if TYPE_CHECKING:
    from src.simulate import Simulator


@dataclass
class NoiseModel(ABC):
    """Abstract base class from which noise models are extended."""

    @abstractmethod
    def __call__(self, observed: np.ndarray, simulator: "Simulator", theta: np.ndarray) -> float:
        """Return the log likelihood of a set of parameters.

        Parameters
        ----------
        observed
            Vector of observations.
        simulator
            `Simulator` instance.
        theta
            Parameters of interest.
        """
        pass


def _gaussian_likelihood(obs, sim, var):
    diff = obs - sim
    return -0.5 * (len(diff) * np.log(2 * np.pi * var) + (diff @ diff) / var)


def _gaussian_likelihood_fd(obs, sim, var):
    diff = obs - sim
    var *= len(diff)
    return -0.5 * (
        len(diff) * np.log(2 * np.pi * var) + (np.real(diff) @ np.real(diff) + np.imag(diff) @ np.imag(diff)) / var
    )


@dataclass
class GaussianNoise(NoiseModel):
    """Class defining a Gaussian noise model with a known variance."""

    var: float

    def __call__(self, observed, simulator, theta) -> float:
        return _gaussian_likelihood(observed, simulator(theta), self.var)


@dataclass
class GaussianNoiseLogVar(NoiseModel):
    """Class defining a Gaussian noise model with a variance defined by the parameter vector."""

    def __call__(self, observed, simulator, theta) -> float:
        return _gaussian_likelihood(observed, simulator(theta), 10 ** simulator.converter.noise_var(theta))


@dataclass
class GaussianNoiseLogVarFD(GaussianNoiseLogVar):
    """Class defining a frequency domain Gaussian noise model with a variance defined by the parameter vector."""

    def __call__(self, observed, simulator, theta) -> float:
        return _gaussian_likelihood_fd(observed, simulator(theta), 10 ** simulator.converter.noise_var(theta))
