from typing import Tuple

import numpy as np


def split_modal_params(params: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    return np.array_split(params[..., : params.shape[-1] - params.shape[-1] % 3], 3, axis=-1)


def alpha_to_Phi(alpha: np.ndarray, exc_point: int) -> np.ndarray:
    p = alpha.shape[0]
    Phi = np.zeros((p, p))
    Phi[exc_point, :] = alpha
    return Phi
