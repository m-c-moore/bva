import enum


class MeasureType(enum.Enum):
    dis = 0
    vel = 1
    acc = 2
