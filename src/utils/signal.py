from typing import Optional

import numpy as np
import pyfftw as pyfftw
from numba import jit


@jit(nopython=True)
def to_db(y: np.ndarray) -> np.ndarray:
    """Return `20*log|y|`."""
    return 20 * np.log10(np.abs(y))


def fft(y: np.ndarray, out: Optional[np.ndarray] = None) -> np.ndarray:
    """Return the FFT of `y`, using pyfftw for speed. Output is of length `len(y) // 2 + 1`.

    Parameters
    ----------
    y
        1D input signal. Will be converted to "float32".
    out
        If provided, will be filled with the fft output. Must be of type "complex64".
    """
    pyfftw.forget_wisdom()
    fft_in = pyfftw.empty_aligned(y.shape[0], dtype="float32")
    if out is None:
        out = pyfftw.empty_aligned(y.shape[0] // 2 + 1, dtype="complex64")
    fft_obj = pyfftw.FFTW(fft_in, out, flags=("FFTW_ESTIMATE", "FFTW_DESTROY_INPUT"))
    fft_in[:] = np.array(y, dtype="float32")
    return fft_obj()


def i_fft(Y: np.ndarray, out: Optional[np.ndarray] = None) -> np.ndarray:
    """Return the inverse FFT of `y``, using pyfftw for speed. Output is of length `2 * len(y) - 2`.

    Parameters
    ----------
    Y
        1D input signal. Will be converted to "complex64".
    out
        If provided, will be filled with the output. Must be of type "float32".
    """
    pyfftw.forget_wisdom()
    fft_in = pyfftw.empty_aligned(Y.shape[0], dtype="complex64")
    if out is None:
        out = pyfftw.empty_aligned(Y.shape[0] * 2 - 2, dtype="float32")
    fft_obj = pyfftw.FFTW(fft_in, out, direction="FFTW_BACKWARD", flags=("FFTW_ESTIMATE", "FFTW_DESTROY_INPUT"))
    fft_in[:] = np.array(Y, dtype="complex64")
    return fft_obj()


def fft_axis(t: np.ndarray) -> np.ndarray:
    return np.linspace(0, np.pi / (t[1] - t[0]), len(t) // 2 + 1)
